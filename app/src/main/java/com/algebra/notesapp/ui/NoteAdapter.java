package com.algebra.notesapp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.algebra.notesapp.R;
import com.algebra.notesapp.database.Note;

import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {

    public NoteAdapter(Context context, List<Note> note) {
        super(context, 0, note);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note note = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        if(note != null) {
            TextView tvTitle = convertView.findViewById(R.id.tvTitle);
            TextView tvNote = convertView.findViewById(R.id.tvNote);
            tvTitle.setText(note.getTitle());
            tvNote.setText(note.getNote());
        }

        return convertView;
    }
}
