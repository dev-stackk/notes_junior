package com.algebra.notesapp.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.algebra.notesapp.MainActivity;
import com.algebra.notesapp.R;
import com.algebra.notesapp.database.Note;
import com.algebra.notesapp.database.NoteDatabase;

public class Dialog extends DialogFragment {

    private Note note;

    private EditText etNote;
    private EditText etTitle;
    private Button bUpdate;
    private Button bCancel;
    private Button bDelete;

    private NoteDatabase noteDatabase;

    public Dialog(Note note) {
        this.note = note;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        noteDatabase = NoteDatabase.getInstance(getActivity());
    }

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialog = inflater.inflate(R.layout.dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialog);
        initWidgets(dialog);
        populateFields(this.note);
        setupListeners();
        return builder.create();
    }

    private void setupListeners() {
        bCancel.setOnClickListener(view -> {
            dismiss();
        });

        bUpdate.setOnClickListener(view -> {
            this.note.setNote(etNote.getText().toString());
            this.note.setTitle(etTitle.getText().toString());
            noteDatabase.getNoteDao().update(this.note);
            dismiss();
        });

        bDelete.setOnClickListener(view -> {
            noteDatabase.getNoteDao().deleteById(this.note.getId());
            dismiss();
        });
    }

    private void populateFields(Note note) {
        etTitle.setText(note.getTitle());
        etNote.setText(note.getNote());
    }

    private void initWidgets(View dialog) {
        etNote = dialog.findViewById(R.id.etDialogNote);
        etTitle = dialog.findViewById(R.id.etDialogTitle);
        bCancel = dialog.findViewById(R.id.bCancel);
        bUpdate = dialog.findViewById(R.id.bUpdate);
        bDelete = dialog.findViewById(R.id.bDelete);
    }

    @Override
    public void onDetach() {
        MainActivity.notifyDataChanged();
        super.onDetach();
    }
}