package com.algebra.notesapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.algebra.notesapp.database.Note;
import com.algebra.notesapp.database.NoteDatabase;
import com.algebra.notesapp.ui.Dialog;
import com.algebra.notesapp.ui.NoteAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText etTitle;
    private EditText etNote;
    private Button bAdd;

    private static NoteDatabase noteDatabase;
    private static ListView list;
    private static NoteAdapter noteAdapter;
    private static List<Note> notes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        setupData();
        setupListeners();
    }

    private void initWidgets() {
        noteDatabase = NoteDatabase.getInstance(this);
        etNote = findViewById(R.id.etNote);
        etTitle = findViewById(R.id.etTitle);
        list = findViewById(R.id.lvResults);
        bAdd = findViewById(R.id.bSave);
    }

    private void setupData() {
        notes.addAll(noteDatabase.getNoteDao().getAll());
        noteAdapter = new NoteAdapter(this, notes);
        list.setAdapter(noteAdapter);
    }

    private void setupListeners() {
        bAdd.setOnClickListener(view -> {
            String title = etTitle.getText().toString();
            String note = etNote.getText().toString();

            noteDatabase.getNoteDao().insert(new Note(note, title));
            notifyDataChanged();
        });

        list.setOnItemClickListener((adapterView, view, i, l) -> {
            System.out.println("icl" + i + ":" + l);

            Dialog dialog = new Dialog(notes.get(i));
            dialog.show(getSupportFragmentManager(), "NOTE");
        });
    }

    public static void notifyDataChanged() {
        notes.clear();
        notes.addAll(noteDatabase.getNoteDao().getAll());
        ((BaseAdapter) list.getAdapter()).notifyDataSetChanged();
    }

}
