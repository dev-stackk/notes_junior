package com.algebra.notesapp.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM Note ")
    List<Note> getAll();

    @Query("SELECT * FROM Note WHERE id=:id")
    Note getNoteById(long id);

    @Insert
    void insert(Note note);

    @Update
    void update(Note noteForUpdate);

    @Delete
    void delete(Note note);

    @Query("DELETE FROM Note WHERE id=:id")
    void deleteById(long id);

    @Delete
    void delete(Note... note);

}