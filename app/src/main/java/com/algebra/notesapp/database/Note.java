package com.algebra.notesapp.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Note {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String note;
    private String title;

    public Note() {}

    public Note(String note, String title) {
        this.note = note;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Note{" +
                "note_id=" + id +
                ", note='" + note + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
